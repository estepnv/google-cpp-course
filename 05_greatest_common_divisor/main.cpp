// greatest_common_devisor - Evgeny Stepanov
// Description: Euclidean algorith GCD implementation, recursive and using loop.

#include "iostream"
#include "math.h"

using namespace std;


int rgcd(int num_1, int num_2) {
    if (num_2 == 0) {
        return num_1;
    }
    
    int leftover = -1, temp = -1;

    if (abs(num_2) > abs(num_1)) {
        temp = num_1;
        num_1 = num_2;
        num_2 = temp;
    } 

    leftover = num_1 % num_2;

    return rgcd(num_2, leftover);
}


int gcd(int num_1, int num_2) {
    int leftover = -1, temp;

    if (abs(num_2) > abs(num_1)) {
        temp = num_1;
        num_1 = num_2;
        num_2 = temp;
    } 

    do {
        leftover = num_1 % num_2;

        num_1 = num_2;
        num_2 = leftover;

    } while (leftover != 0);

    return num_1;
}

int get_number() {
    int val = -1;
    cin >> val;
    cin.clear();
    cin.ignore(10000, '\n');

    return val;
}

int main() {
    cout << "Input 2 numbers:\n";
    int num_1 = get_number(), num_2 = get_number();

    cout << "Loop implementation: gcd: " << gcd(num_1, num_2) << endl;
    cout << "Recursive implementation: gcd: " << rgcd(num_1, num_2) << endl;
 
    return 0;
}
