// get_input - Evgeny Stepanov
// Description: a program that reads user input and prints it

#include "iostream"

using namespace std;

int main(int argc, char** args) {
    int input_var = 0;

    do {
        cout << "Enter a number ( -1 is Exit )\n";


        if ( !(cin >> input_var) ) {
            cout << "You entered a non-numeric.\n";

            if (cin.fail()) {
                cin.clear();
            }

            cin.ignore(10000, '\n');
        } else if ( input_var != -1 ) {
            cout << "You entered: " << input_var << endl;
        }

    } while (input_var != -1);

    cout << "All done .. Bye";

    return 0;
}
