// helloworld - Evgeny Stepanov
// Description: a program that prints immortal saying "hello world"

#include "iostream"

using namespace std;

int main(int argc, char** args) {
    char space = ' ';

    for(int lines = 0; lines < 6; lines++) {

        for (int times = 0; times < 4; times++) {
            cout.flags ( std::ios::left );
            cout.width (17);
            cout.fill (space);
            cout << "Hello world";
        }

        cout << "\n";
        
    }
    
    return 0;
}
