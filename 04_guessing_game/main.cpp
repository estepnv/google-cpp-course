// guessing_game - Evgeny Stepanov
// Description: A guessing game. A player should try to guess a random 0..200 number using hints

#include "iostream"
#include "stdlib.h"

using namespace std;

int random_number() {
    return rand() % 200;
}

int get_input() {
    int input = 0;

    cout << "Try to guess a number from 0 to 200! :)\n> ";

    
    if ( !(cin >> input) ) {
        if ( cin.fail() ) {
            cin.clear();
        }
        cin.ignore(10000, '\n');
    }

    return input;
}

bool check(int input, int number) {
    

    if ( input == number ) {
        cout << "Congratulations! Game over :)\n";
        return true;
    }

    if ( input > number ) {
        cout << "That's too high :(\n";
    } else {
        cout << "That's too low :(\n";
    }

    return false;
}

void game() {
    int magic_number = random_number();
    int user_input = -1;

    do {
        user_input = get_input();
    } while (!check(user_input, magic_number));
}

int main(int argc, char** args) {
    srand(unsigned(time(0)));
    
    game();

    return 0;
}
