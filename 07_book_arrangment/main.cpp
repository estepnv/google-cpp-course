// book_arrangement - Evgeny Stepanov
// Description: A books arrangement task

#include "iostream"
#include "vector"
#include "iterator" 

using namespace std;

template <class Iterator>
void print_arr(Iterator begin, Iterator end) {
    for (auto p = begin; p < end; p ++ )
        cout << *p << " ";

    cout << endl;
}

template <class T>
void print_arr(std::vector<T> vector) {
    print_arr(vector.begin(), vector.end());
}

template <class Iterator, class ItValue = typename std::iterator_traits<Iterator>::value_type>
void swap(Iterator val1, Iterator val2) {
    ItValue bucket = *val1;
    *val1 = *val2;
    *val2 = bucket;
}

template <class Iterator, class ItValue = typename std::iterator_traits<Iterator>::value_type>
void reverse(Iterator begin, Iterator end) {
    int len = end - begin;
    auto p = begin;
    int half = len / 2;

    for (int i = 0; i < half ; i++) {
        ::swap(p + i, end - 1 - i);
    }
}

template <class Iterator>
bool next_permutation(Iterator begin, Iterator end) {
    if (begin == end) return false;
    auto curr = end - 1;
    if (begin == curr) return false;

    while (true) {
        auto prev = curr--, k = end;

        if ( *curr < *prev ) {
            while( *curr >= *--k );

            ::swap(curr, k);
            ::reverse(prev, end);
            return true;
        }

        if ( curr == begin ) {
            ::reverse(begin, end);
            return false;
        }
    }
}

int fact(int n) {
    return n == 0 ? 1 : n * fact(n - 1);
}

template <class Iterator>
vector<vector<int>> permutation(Iterator begin, Iterator end) {
    int size = end - begin;

    vector<vector<int>> result;

    if (size < 2) {
        auto entry = vector<int>(1, *begin);
        result.push_back(entry);
        return result;
    }

    int entry = *begin;

    for (auto perm : permutation(begin + 1, end)) {
        for (auto it = 0; it <= perm.size(); it++) {
            vector<int> perm_copy;
            for (auto entry : perm) perm_copy.push_back(entry);
            perm_copy.insert(perm_copy.begin() + it, 1, entry);
            result.push_back(perm_copy);
        }
    }

    return result;
}

int main(int argc, char** args) {
    vector<int> books = { 1, 2, 3, 4, 5, 6, 7 };

    int counter = 1;

    while(::next_permutation(books.begin(), books.end())) ++counter;
    cout << "possible ongoing permutations: " << counter << endl;

    auto res =  permutation(books.begin(), books.end());
    cout << "possible precalculated permutations: " << res.size() << endl;

    return 0;
}
