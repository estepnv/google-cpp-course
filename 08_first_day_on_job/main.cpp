// first_day_on_job - Evgeny Stepanov
// Description: A program which computes wich sallary type to pick depending on weekly sales

#include "iostream"
#include "vector"
#include "iterator"

#define SHOE_COST 225.0
#define HOURS_IN_WEEK 40
#define METHOD_2_HOURLY_SALARY 0.1
#define METHOD_2_COMMISSION 0.1
#define METHOD_3_COMMISSION 0.2
#define METHOD_3_BONUS_PER_PAIR 20.0

using namespace std;

float get_input() {
    cout << "Enter weekly sales:\n> ";
    float input = 0.0;
    cin >> input;
    cin.clear();
    cin.ignore(10000, '\n');

    return input;
}

void calc_method_1(float weekly_sales) {
    cout << "Method 1 salary: 600.0\n";
}

void calc_method_2(float pairs_sold) {
    float salary = METHOD_2_HOURLY_SALARY * HOURS_IN_WEEK + pairs_sold * SHOE_COST * METHOD_2_COMMISSION;
    cout << "Method 2 salary: " << salary << endl;
}

void calc_method_3(float pairs_sold) {
    float salary = pairs_sold * METHOD_3_BONUS_PER_PAIR + pairs_sold * SHOE_COST * METHOD_2_COMMISSION;
    cout << "Method 3 salary: " << salary << endl;
}

int main(int argc, char** args) {
    auto weekly_sales = vector<float>(7, 0.0);

    float pairs_sold = get_input();

    calc_method_1(pairs_sold);
    calc_method_2(pairs_sold);
    calc_method_3(pairs_sold);

    return 0;
}
